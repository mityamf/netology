var inCur = $('#input_currency'),
	outCur = $('#output_currency'),
	inpValue = $('#input_value'),
	outValue = $('#output_value');

$(document).ready(function() {
	outCur.append($('<option value="1" data-nominal="1">RUB</option>'));
	$.getJSON( "http://university.netology.ru/api/currency", function(array) {
		$.each(array, function(index, object) {
//			inCur.append($('<option value="' + object.Value + '">' + object.CharCode + '</option>'));
			inCur.append($('<option value="' + object.Value + '"' + 'data-nominal="' + object.Nominal + '">' + object.CharCode + '</option>'));
			outCur.append($('<option value="' + object.Value + '"' + 'data-nominal="' + object.Nominal + '">' + object.CharCode + '</option>'));
			console.log(object);

		});
	$('#input_currency :contains("USD")').attr('selected', 'selected');
	
	calcOutputAmount();
	});
});

inCur.on("change", calcOutputAmount);
outCur.on("change", calcOutputAmount);
inpValue.on("keyup", calcOutputAmount);
outValue.on("keyup", recalcInputAmount);

function calcOutputAmount() {
	var nominalIn = $("#input_currency option:selected").data("nominal"),
		nominalOut = $("#output_currency option:selected").data("nominal");
	outValue.val( inCur.val() * inpValue.val() * nominalOut/ ( nominalIn*outCur.val() ) );
};

function recalcInputAmount() {
	var nominalIn = $("#input_currency option:selected").data("nominal"),
		nominalOut = $("#output_currency option:selected").data("nominal");
	inpValue.val( outValue.val() * nominalIn * outCur.val() / ( inCur.val() * nominalOut ));
};

inpValue.keypress(function(e) {
	if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
	return false;
	}
});

