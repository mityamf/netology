// Задание 1 - работа со списком

var list = document.querySelectorAll(".form-group ul li");

for (var i = 0; i < list.length; i++) {
	list[i].addEventListener("click", deselect);
	list[i].addEventListener("click", clickList);

}

function deselect(e) {
	if ( e.ctrlKey ) { return };
	for (var i = 0; i < this.parentNode.children.length; i++) {
		this.parentNode.children[i].style.background = "";
	}

}

function clickList(e) {
	e.target.style.background = "#b833ff";
	//this.style.background = "#b833ff";
}

// Задание 2 - подсчет символов в поле Пароль

$("#exampleInputPassword1").keyup($.debounce(500, onKeyUp));
var parent = document.getElementById("exampleInputPassword1").parentNode,
	hint = document.createElement("p");
parent.appendChild(hint);

function onKeyUp(e) {
	var badPass = "Пароль должен быть длиннее 5 символов",
		goodPass = "Хороший пароль";

	if (e.originalEvent.target.value.length <= 5) {
		hint.style.color = "red";
		hint.innerText = badPass;
	}
	else {
		hint.style.color = "green";
		hint.innerText = goodPass;	
	}

}

// Задание 3 - подсвечивание области при наведении

var infoBlock = document.getElementById("helpBlock").parentNode;
infoBlock.onmouseenter = makeBorder;
infoBlock.onmouseleave = removeBorder;


function makeBorder() {
	infoBlock.style.boxShadow = "0 0 8px rgb(102, 175, 233)";
}
function removeBorder() {
	infoBlock.style.boxShadow = "";
}


// Задание с формой

var btn = document.querySelector("form button");
btn.addEventListener("click", clickHandler);

function clickHandler(e) {
	e.preventDefault();
	var firstName = document.getElementById("firstName"),
		lastName = document.getElementById("lastName"),
		password = document.getElementById("exampleInputPassword1");

	//var isEntered = !!firstName.value && !!lastName.value && !!password.value;
	var result = "Имя: " + firstName.value + "\n" + "Фамилия: " + lastName.value + "\n" + "Пароль: " + password.value;
	
	if ( e.shiftKey ) {
		alert(result);
		return;
	}

	console.log(result);


}

// Задание c перемещением слайдера

var slider = document.getElementById("slider");
var thumb = document.querySelector(".thumb");

thumb.onmousedown = function(e) {
	document.onmousemove = function(e) {
	    var coordLeft = e.pageX - slider.getBoundingClientRect().left - thumb.offsetWidth / 2;
		if (coordLeft < 0) {
	    	coordLeft = 0;
	    };
	    var rightBorder = slider.offsetWidth - thumb.offsetWidth;
	    if (coordLeft > rightBorder) {
	    	coordLeft = rightBorder;
	    };

	    thumb.style.left = coordLeft + "px";
  	};

  	document.onmouseup = function() {
    	document.onmousemove = null;
    	document.onmouseup = null;
  };

};
