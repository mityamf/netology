var form = document.querySelector("form[data-role=xml-request-form"),
	email = document.querySelector("input[data-role=xml-request-form-login]"),
	password = document.querySelector("input[data-role=xml-request-form-password]"),
	submit = document.querySelector("button[data-role=xml-request-form-submit]");
	userProfile = document.querySelector("div[data-role=xml-request-user-profile]"),
	errorBlock = document.querySelector("div[data-role=xml-request-error"),
	signOut = document.querySelector("button[data-role=xml-request-user-profile-logout"),
	preloader = document.querySelector("div[data-role=loader]"),
	loginForm = document.querySelector("div[data-role=user-form]"),
	avatar = document.querySelector("img[data-role=xml-request-user-profile-avatar"),
	fullName = document.querySelector("span[data-role=xml-request-user-profile-fullname]"),
	country = document.querySelector("span[data-role=xml-request-user-profile-country]"),
	hobbies = document.querySelector("span[data-role=xml-request-user-profile-hobbies]");

var xhr = new XMLHttpRequest();

submit.addEventListener("click", sendResults);

function sendResults(e) {
	e.preventDefault();
	toggleObjectVisibility(true, preloader, false, loginForm, false, errorBlock);
	var body = JSON.stringify({
		email: email.value,
		password: password.value
	});
	var action = form.action;
	xhr.open("POST", action);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(body);
}

xhr.onload = function() {
	var response = xhr.responseXML;
	if (xhr.readyState !== XMLHttpRequest.DONE ) return;
	if (xhr.status == 200 ) {
		toggleObjectVisibility(false, preloader, false, loginForm, true, userProfile);
		writeAnswer(response);
	} else {
		toggleObjectVisibility(false, preloader, true, loginForm, true, errorBlock);
		errorBlock.innerText = "Error " + xhr.status + ": " + response.getElementsByTagName("error")[0].children[0].textContent;
	}
}

function writeAnswer(response) {
	fullName.innerText = response.getElementsByTagName("name")[0].textContent + " " + response.getElementsByTagName("lastname")[0].textContent;
	country.innerText = response.getElementsByTagName("country")[0].textContent;
	avatar.src = response.getElementsByTagName("userpic")[0].textContent;
	var hobbiesArr = response.getElementsByTagName("hobbies")[0].children;
	var hobbiesString = "";
	for (var i = 0; i < hobbiesArr.length; i++) {
		hobbiesString += hobbiesArr[i].textContent + ", ";
	}
	hobbies.innerText = hobbiesString.slice(0, -2);
}
/*
function writeError(response) {

}
*/

function toggleObjectVisibility(){
	//first argument is boolean (show or hide), second one - object
	for (var i = 0; i < arguments.length; i+=2) {
	    if (arguments[i])
	        arguments[i+1].classList.remove("hide");
	    else
	        arguments[i+1].classList.add("hide");
	}
}

signOut.onclick = function() {
	toggleObjectVisibility(true, loginForm, false, userProfile);
}