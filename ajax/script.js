var form = document.querySelector("form[data-role=json-request-form"),
	email = document.querySelector("input[data-role=json-request-form-login]"),
	password = document.querySelector("input[data-role=json-request-form-password]"),
	submit = document.querySelector("button[data-role=json-request-form-submit]");
	userProfile = document.querySelector("div[data-role=json-request-user-profile]"),
	errorBlock = document.querySelector("div[data-role=json-request-error"),
	signOut = document.querySelector("button[data-role=json-request-user-profile-logout"),
	preloader = document.querySelector("div[data-role=loader]"),
	loginForm = document.querySelector("div[data-role=user-form]"),
	avatar = document.querySelector("img[data-role=json-request-user-profile-avatar"),
	fullName = document.querySelector("span[data-role=json-request-user-profile-fullname]"),
	country = document.querySelector("span[data-role=json-request-user-profile-country]"),
	hobbies = document.querySelector("span[data-role=json-request-user-profile-hobbies]");

var xhr = new XMLHttpRequest();

submit.addEventListener("click", sendResults);

function sendResults(e) {
	e.preventDefault();
	toggleObjectVisibility(true, preloader, false, loginForm, false, errorBlock);
	var body = "email=" + encodeURIComponent(email.value) + "&password=" + encodeURIComponent(password.value);
	var action = form.action;
	xhr.open("POST", action);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.send(body);
}

xhr.onload = function() {
	var response = JSON.parse(xhr.response);
	if (xhr.readyState !== XMLHttpRequest.DONE ) return;
	if (xhr.status == 200 ) {
		toggleObjectVisibility(false, preloader, false, loginForm, true, userProfile);
		writeAnswer(response);
	} else {
		toggleObjectVisibility(false, preloader, true, loginForm, true, errorBlock);
		errorBlock.innerText = "Error " + response.error.code + ": " + response.error.message;
	}
}

function writeAnswer(response) {
	fullName.innerText = response.name + " " + response.lastname;
	country.innerText = response.country;
	hobbies.innerText = response.hobbies;
	avatar.src = response.userpic;

}

function toggleObjectVisibility(){
	//first argument is boolean (show or hide), second one - object
	for (var i = 0; i < arguments.length; i+=2) {
	    if (arguments[i])
	        arguments[i+1].classList.remove("hide");
	    else
	        arguments[i+1].classList.add("hide");
	}
}

signOut.onclick = function() {
	toggleObjectVisibility(true, loginForm, false, userProfile);
}