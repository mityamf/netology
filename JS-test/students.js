var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

//Вывод списка студентов, пункт 1 задания
console.log("Список студентов:");

for (var i = 0, imax = studentsAndPoints.length; i < imax; i+= 2) {
    console.log( "Студент", studentsAndPoints[i], "набрал", studentsAndPoints[i+1], "баллов");
}

//Вывод студента с максимальным баллом, пункт 2 задания
var bestStudent, highestScore = -1;

for (i = 0, imax = studentsAndPoints.length; i < imax; i+= 2) {
    if ( highestScore < studentsAndPoints[i+1] ) {
        highestScore = studentsAndPoints[i+1];
        bestStudent = studentsAndPoints[i];
    }
}

console.log("\nСтудент набравший максимальный балл:");
console.log( "Студент", bestStudent, "имеет максимальный бал", highestScore);

//Добавили данные по новым студентам, пункт 3 задания
studentsAndPoints.push("Николай Фролов", 0, "Олег Боровой", 0);


// Добавление очков выбранным студентам, пункт 4 задания (добавлена проверка на существование)

var extraPointsWithNames = ["Антон Павлович", 10, "Николай Фролов", 10, "Вася Петров", 10];
var nameIndex;
for (i = 0, imax = extraPointsWithNames.length; i < imax; i+= 2) {
    nameIndex = studentsAndPoints.indexOf(extraPointsWithNames[i]);
    if (nameIndex >= 0) {
        studentsAndPoints[nameIndex + 1] += extraPointsWithNames[i+1];
    }
    else {
        console.log("Студент", extraPointsWithNames[i], "не числится в списке");
    }
    
}


//Вывод студентов, не набравших баллов, пункт 5 задания
console.log("\nСтуденты, не набравшие баллов:");
for (i = 0, imax = studentsAndPoints.length; i < imax; i+= 2) {
    if ( studentsAndPoints[i+1] == 0 ) {
        console.log( studentsAndPoints[i] );
    }
}


// Удаление нулевых результатов, дополнительное задание, вариант №2


var zeroIndex;
while ( (zeroIndex = studentsAndPoints.indexOf(0)) >= 0 ) {
    studentsAndPoints.splice(zeroIndex-1, 2);
}

console.log("\nСтуденты с ненулевыми оценками:")
for (i = 0, imax = studentsAndPoints.length; i < imax; i+= 2) {
    console.log( "Студент", studentsAndPoints[i], "набрал", studentsAndPoints[i+1], "баллов");
}


/*
var zeroIndex = []; // массив, где элементы - это имена студентов с нулевой оценкой из массива studentsAndPoints

for (i = 0, imax = studentsAndPoints.length; i < imax; i+= 2) {
    if ( studentsAndPoints[i+1] == 0 ) {
        zeroIndex.push(studentsAndPoints[i]); // эту часть можно было бы сделать и на предыдущем шаге
    }
}
var nameIndex;
for (i = 0, imax = zeroIndex.length; i < imax; ++i) {
    nameIndex = studentsAndPoints.indexOf(zeroIndex[i]);
    studentsAndPoints.splice(nameIndex, 2);
}
console.log("\nСтуденты с ненулевыми оценками:")
for (i = 0, imax = studentsAndPoints.length; i < imax; i+= 2) {
    console.log( "Студент", studentsAndPoints[i], "набрал", studentsAndPoints[i+1], "баллов");
}
*/