"use strict";
var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

// Задание 1 - формирование массива объектов и вывод его в консоль
var students = [];
var show = function() {
	console.log( "Студент %s набрал %d баллов", this.name, this.point );
	};

for (var i = 0, imax = studentsAndPoints.length; i < imax; i+=2) {
	students.push({
		name: studentsAndPoints[i],
		point: studentsAndPoints[i+1],
		show: show
		});
};

console.log("Вывод единичного значения элемента массива");
students[1].show();

// Функция вывода всех студентов - для себя
function showAll(arr) {
	arr.forEach(function(item, i, arr) {
		if ( "show" in arr[i]) {
			arr[i].show();
		}
	});
};

// Задание 2 - добавление новых студентов
var newStudents = [
	{name: "Николай Фролов", point: 0, show: show},
	{name: "Олег Боровой", point: 0, show: show}
	];

students = students.concat(newStudents);
console.log("\nОбновленный список студентов:");
showAll(students);


// Задание 3 - увеличение баллов студентам
var additionalPoint = [
	{name: "Ирина Овчинникова", point: 30},
	{name: "Александр Малов", point: 30},
	{name: "Николай Фролов", point: 10}
];

additionalPoint.forEach(function(additionalItem) {
	var foundObject;
	foundObject = students.find(function(item) {
		return additionalItem.name == item.name;
	});	
	if ( foundObject !== undefined ) 
		{ foundObject.point += additionalItem.point;
		}
	else { console.warn("Попытка добавить баллов студенту '%s'. Студент не найден", additionalItem.name); }
});
console.log("\nОценки студентов после увеличения");
showAll(students);

// Задание 4 - вывод студентов с оценками >=30
console.log("\nСтуденты с оценками 30 и более баллов:")
students.forEach(function(item) {
	if (item.point >= 30 ) item.show();
});

// Задание 5 - добавление дополнительного поля

students.forEach(function(item) {
	item.worksAmount = item.point / 10;
});

// Дополнительное задание - добавление метода findByName к students

students.findByName = function(name) {
	// this - весь массив, к которому применяется метод
	return this.find(function(item) {	
		return name == item.name;
		});
	
};

var looking = students.findByName("Глеб Стукалов");
console.log("\nОбъект с именем '%s' найден", looking.name);
