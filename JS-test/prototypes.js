// Задание 1 - создание конструктора индивид. студента
function Student(name, points) {
	this.name = name;
	this.points = points;
};

Student.prototype.show = function() {
		console.log( "Студент %s набрал %d баллов", this.name, this.points);
};


/* Задание 2 - создание конструктора группы студентов, где 
	groupName - название группы
	students - массив студентов, где каждый студент - объект, созданный конструктором из задания 1
	add - метод объекта, добавляющий нового студента в массив students
*/
function StudentList(groupName, studentsArray) {
	this.groupName = groupName;
	var students = [];
	if (studentsArray !== undefined) {
	for (var i = 0, imax = studentsArray.length; i < imax; i+=2) {
		students.push(new Student(studentsArray[i], studentsArray[i+1]));
	}};
	this.students = students;

};

StudentList.prototype.add = function(name, points) {
		this.students.push(new Student(name, points));
	};

// Задание 3 - создание списка студентов hj2
var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];
var hj2 = new StudentList("HJ-2", studentsAndPoints);

// Задание 4 - добавление новых студентов в группу HJ-2
var additionalStudentsArr = ["Петя Иванов", 20, "Вася Пупкин", 0, "Элаиза Рябушинская", 40];
for (var i = 0, imax = additionalStudentsArr.length; i < imax; i+=2) {
	hj2.add(additionalStudentsArr[i], additionalStudentsArr[i+1]);
	};

// Задание 5 - создание новой пустой группы студентов и последующее добавление в неё новых студентов
var html7 = new StudentList("HTML7");
var newStudentsArr = ["Вася Васечкин", 10, "Эльвира Набиуллина", 90, "Олег Попов", 0, "Кристина Полозова", 40];

for (var i = 0, imax = newStudentsArr.length; i < imax; i+=2) {
	html7.add(newStudentsArr[i], newStudentsArr[i+1]);
	};

// Задание 6 - добавление метода show ко спискам студентов

StudentList.prototype.show = function() {
	console.log("\nГруппа '%s' (%d студентов):", this.groupName, this.students.length);
	for (var i = 0; i < this.students.length; i++) {
		this.students[i].show();
	};
};

// Задание 7 - перевести студента из одной группы в другую
hj2.students.findByName = function(name) {
	return this.find(function(item) {	
		return name == item.name;
		});
	
};
var studentToTransfer = hj2.students.findByName("Вася Пупкин");
html7.add(studentToTransfer.name, studentToTransfer.points);
hj2.students.splice(hj2.students.indexOf(studentToTransfer), 1);
