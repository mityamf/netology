// Сегодня мы печем пироги!

// Вам нужно испечь по 10 пирогов каждого типа
// Можно использовать все знания, которые вы получили на наших лекциях

// Рецепты:
// Яблочный пирог (3 кг яблок, 2 кг. муки, 1 литр молока, 3 яйца)
// Клубничный пирог (5 кг клубники, 1 кг. муки, 2 литр молока, 4 яйца)
// Абрикосовый пирог (2 кг абрикос, 3 кг. муки, 2 литр молока, 2 яйца)

// Запасы на складе
var apples = 20; // Яблоки, кг
var strawberry = 20; // Клубника - 75 р/кг
var apricot = 20; // Абрикос - 35 р/кг
var flour = 20; // Мука - 10 р/кг
var milk = 20; // Молоко - 15 р/литр
var eggs = 50; // Яйца - 3 р/шт
var applePie = 0; // количество яблочных пирогов, которое испекли.
var strawberryPie = 0; // количество клубничных пирогов, которое испекли.
var apricotPie = 0; // количество абрикосовых пирогов, которое испекли.

// Печем яблочный пироги:
function bakeApplePie() {
    if (applePie >= 10) return;
    if (apples - 3 < 0) {
        console.warn('Кончились яблоки! Купили еще 10 кг')
        apples = apples + 10;
    } else if (flour - 2 < 0) {
        console.warn('Кончилась мука! Купили еще 10 кг')
        flour = flour + 10;
    } else if (milk - 1 < 0) {
        console.warn('Кончилось молоко! Купили еще 10 литров')
        milk = milk + 10;
    } else if (eggs - 3 < 0) {
        console.warn('Кончились яйца! Купили еще 10 штук')
        eggs = eggs + 10;
    } else {
apples = apples - 3
flour = flour - 2
milk = milk - 1
eggs = eggs - 3
    applePie++
    console.info('Яблочный пирог ' +applePie+ ' готов!');
    }
}


function bakeStrawberryPie() {
    // Печем клубничные пироги:
    if (strawberryPie >= 10) return;
    if (strawberry - 5 < 0) {
    console.warn('Кончилась клубника! Купили еще 10 кг')
    strawberry = strawberry + 10;
    } else if (flour - 2 < 0) {
    console.warn('Кончилась мука! Купили еще 10 кг')
    flour = flour + 10;
    } else if (milk - 1 < 0) {
    console.warn('Кончилось молоко! Купили еще 10 литров')
    milk = milk + 10;
    } else if (eggs - 3 < 0) {
    console.warn('Кончились яйца! Купили еще 10 штук')
    eggs = eggs + 10;
    } else {
strawberry = strawberry - 5
flour = flour - 1
milk = milk - 2
eggs = eggs - 4
    strawberryPie++
    console.info('Клубничный пирог '+strawberryPie+' готов!');
    }
}
function bakeApricotPie() {
    // Печем абрикосовые пироги:
    if (apricotPie >= 10) return;
    if (apricot - 2 < 0) {
    console.warn('Кончились абрикосы! Купили еще 10 кг')
    apricot = apricot + 10;
    } else if (flour - 2 < 0) {
    console.warn('Кончилась мука! Купили еще 10 кг')
    flour = flour + 10;
    } else if (milk - 1 < 0) {
    console.warn('Кончилось молоко! Купили еще 10 литров')
    milk = milk + 10;
    } else if (eggs - 3 < 0) {
    console.warn('Кончились яйца! Купили еще 10 штук')
    eggs = eggs + 10;
    } else {
apricot = apricot - 2
flour = flour - 3
milk = milk - 2
eggs = eggs - 2
    apricotPie++
    console.info('Абрикосовый пирог '+apricotPie+' готов!');
    }
}

while ( (applePie < 10) || (strawberryPie < 10) || (apricotPie < 10) ) {
    bakeApplePie();
    bakeStrawberryPie();
    bakeApricotPie();
}
