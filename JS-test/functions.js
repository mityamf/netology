/* Вывод только чётных чисел в консоль в диапазоне от 1 до 100 (включительно) */

var i = 1;
while ( i <= 100 ) {
    if (( i % 2) ==0 ) {
        console.log(i);
    }
    i++;
}


/* Запрос возраста и вывод сообщения в консоль */

var age = prompt("Введите возраст");

if (( age <= 17 ) && ( age > 0 )) {
    console.log("Вы слишком молоды");
}

if (( age <=60 ) && ( age > 17 )) {
    console.log("отличный возраст");
}
if ( age > 60 ) {
    console.log("привет, бабуля!");
}

/* дата и конструкция switch */

var date = prompt("Введите, пожалуйста, дату в формате дд.мм");

switch(date) {
    case "08.03":
        console.log("с 8 марта!");
        break;
    case "23.02":
        console.log("с 23 февраля");
        break;
    case "31.12":
        console.log("с новым годом");
        break;
    default:
        console.log("сегодня просто отличный день и безо всякого праздника");
   
}
