var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

// Задание 1 - сформировать 2 массива

var students = studentsAndPoints.filter(function(item, i) {
	return !(i % 2);
	});

var points = studentsAndPoints.filter(function(item, i) {
	return i % 2;
	});

// Задание 2 - вывод списка студентов с оценками

students.forEach(function(item, i) {
	return console.log("Студент %s набрал %d баллов", item, points[i])
	});

// Задание 3 - найти студента с максимальным балллом и вывести его инфо
var maxPoint, maxIndex;

points.forEach(function (number, i) {
	if (!maxPoint || number > maxPoint) {
		maxPoint = number;
		maxIndex = i;
		}
	});
console.log("Студент, набравший максимальный балл: %s (%d баллов)", students[maxIndex], maxPoint);

// Задание 4 - увеличить баллы студентам. 
var addPointsNames = ["Ирина Овчинникова", "Александр Малов"];
points = points.map(function(item, i) {
	if ( addPointsNames.indexOf(students[i]) >= 0 ) {
		item += 30;
		};
	return item;
	});

// Дополнительное задание
/*
Перебираем массив оценок. Находим максимальную оценку, запоминаем её индекс. 
Делаем push этой максимальной оценки в новый массив. Аналогично поступаем с массивом 
имён студентов, используя запомненный индекс. 
Не забываем удалить данное максимальное значение из нашего временного массива оценок, но удалять нужно с сохранением индекса.
Далее - следующая итерация, будет осуществляться новый поиск максимального значения элементов из оставшихся и т.п.
*/

function getTop(n) {
	var tempArrayOfAllPoints = points.slice(); 
	var pointsTop = [], 
		namesTop = [];
	var currentIteration = 1;
	tempArrayOfAllPoints = tempArrayOfAllPoints.map(function() {
		if (currentIteration > n ) return;
		var maxPoint, maxIndex;
		//поиск максимального значения среди оценок
		tempArrayOfAllPoints.forEach(function (number, i) {
			if (!maxPoint || number > maxPoint) {
				maxPoint = number;
				maxIndex = i;
				};
			});
		delete tempArrayOfAllPoints[maxIndex]; //удаляем значение из массива оценок, чтобы больше оно не фигурировало, при этом сохраняются все индексы
		pointsTop.push(maxPoint);
		namesTop.push(students[maxIndex]);
		currentIteration++;
	
	});
//	pointsTop = pointsTop.slice(0, n);
//	namesTop = namesTop.slice(0, n);
	
	//сливаем оба массива в единый по формату studentsAndPoints
	var studentsAndPointsNew = [];
	namesTop.forEach(function(item,i) {
		studentsAndPointsNew.push(item, pointsTop[i]); 
		});
	return studentsAndPointsNew;
};


var top3 = getTop(3);
console.log("\nТоп 3:")
for (var i = 0, imax = top3.length; i < imax; i+= 2) {
    console.log( top3[i], "-", top3[i+1], "баллов");
}

var top5 = getTop(5);

console.log("\nТоп 5:")
for (var i = 0, imax = top5.length; i < imax; i+= 2) {
    console.log( top5[i], "-", top5[i+1], "баллов");
}
