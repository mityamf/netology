'use strict';

bookApp.controller('SingleBookCtrl', function($scope, $http) {

    $http.get('data/book.json').success(function(data) {
        $scope.book = data;
    });
});