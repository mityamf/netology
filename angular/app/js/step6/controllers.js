'use strict';

bookApp.controller('BookListCtrl', function($scope, $http) {

    $http.get('data/books.json').success(function(data) {
        $scope.books = data;
    });
    $scope.orderProperty = 'price';


});

bookApp.filter('unique', function() {
   return function(collection, keyname) {
      var output = [], 
          keys = [];

      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });

      return output;
   };
});
