// Задание 1 - функция обработки события кнопок

var buttons = document.querySelectorAll("button.btn-success");

for (var i = 0; i < buttons.length; i++) {
	buttons[i].addEventListener( "click", handler);
};

function handler() {
	alert(this.innerHTML + " была нажата");
};

// Задание 2 - обработка кликов по ссылкам.

var links = document.querySelectorAll(".table-bordered a");



for (var i = 0; i < links.length; i++) {

	links[i].addEventListener("click", function(event) {
		event.preventDefault();
		if ( this.parentNode.bgColor !=="" ) {
			this.parentNode.bgColor = "";	
		}
		else {			
			this.parentNode.bgColor = "#cdb5e4";
			};
	});
};

