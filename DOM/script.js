//Задание 1 - изменение цвета фона и текста у ячеек с содержимым "2, 4, 6"

var cells = document.querySelectorAll("#myTable td");
for (var i = 0; i < cells.length; i++) {
	
	if ( (cells[i].textContent.slice(0, -1) % 2) == 0 ) {
		cells[i].style.background ="#333";
		cells[i].style.color ="#fff";
	};
};

// Задание 2 - поменять содержимое двух div местами
var firstDiv = document.getElementById("myDiv");
var firstLine = firstDiv.children[0].children[0].innerText;
var thirdLine = firstDiv.children[0].children[2].innerText;

firstDiv.children[0].children[2].innerText = firstLine;
firstDiv.children[0].children[0].innerText = thirdLine;

// Задание 3 - найти и удалить блок с текстом

var blockForDelete = document.getElementsByClassName("foo bar"); //массив из 3х div
var parent = document.getElementsByClassName("removeSecondBlockHere");
parent[0].removeChild(blockForDelete[1]);